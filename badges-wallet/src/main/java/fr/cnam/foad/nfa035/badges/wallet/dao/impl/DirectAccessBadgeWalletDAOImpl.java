package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;

public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private final ImageFrameMedia<File> media;
    private File walletDatabase;

   private ImageStreamingDeserializer<ResumableImageFrameMedia> deserializer;

  private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);


   public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {

      this.walletDatabase = new File(dbPath);
       this.media = new ImageFileFrame(walletDatabase);
   }

   @Override
   public void addBadge(File image) throws Exception {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {

          ImageStreamingSerializer serializer = new ImageSerializerBase64DatabaseImpl();
          serializer.serialize(image, media);

      }
   }

   @Override
   public void getBadge(OutputStream imageStream) throws Exception {
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
        new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
      }
   }



    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
           // new ImageDeserializerBase64DatabaseImpl((OutputStream) deserializer).deserialize(media);
           // new ImageDeserializerBase64DatabaseImpl(List<DigitalBadgeMetadata>).transferTo(walletDatabase);
        }


        return null;
    }

   @Override
   public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws Exception {
      List<DigitalBadgeMetadata> metadataList = this.getWalletMetadata();
      try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
          new ImageDeserializerBase64DatabaseImpl((OutputStream) deserializer).deserialize(media);
         // return metas;
      }
   }


}
