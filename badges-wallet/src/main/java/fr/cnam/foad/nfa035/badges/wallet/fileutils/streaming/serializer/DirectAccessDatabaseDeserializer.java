package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;

public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia> {
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException;
    <T extends OutputStream> T getSourceOutputStream();
    <T extends OutputStream> void setSourceOutputStream(T os);
}
