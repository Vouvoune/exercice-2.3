package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.File;
import java.io.OutputStream;
import java.util.List;

public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {


    void addBadge(File image) throws Exception;



    void getBadge(OutputStream imageStream) throws Exception ;

    List<DigitalBadgeMetadata> getWalletMetadata() throws Exception;


    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws Exception;
}
