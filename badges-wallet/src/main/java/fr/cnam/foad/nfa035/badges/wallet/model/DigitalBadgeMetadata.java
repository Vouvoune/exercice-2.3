package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

public class DigitalBadgeMetadata {


    int badgeId;
  long imageSize;
  int walletPosition;



    public DigitalBadgeMetadata(int badgeId, long imageSize, int walletPosition) {
        this.badgeId=badgeId;
        this.imageSize=imageSize;
        this.walletPosition=walletPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadgeMetadata)) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return getBadgeId() == that.getBadgeId() && getImageSize() == that.getImageSize() && getWalletPosition() == that.getWalletPosition();
    }

    public int getBadgeId() {
        return badgeId;
    }


    public long getImageSize() {
        return imageSize;
    }


    public long getWalletPosition() {
        return walletPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBadgeId(), getImageSize(), getWalletPosition());
    }

    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    public void setWalletPosition(int walletPosition) {
        this.walletPosition = walletPosition;
    }

    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeId=" + badgeId +
                ", imageSize=" + imageSize +
                ", walletPosition=" + walletPosition +
                '}';
    }

}
