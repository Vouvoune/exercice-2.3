package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DigitalBadge {

    File badge;
    private List<DigitalBadgeMetadata> metadata;
    List<DigitalBadgeMetadata> DigitalBadgeMetadata=new ArrayList<DigitalBadgeMetadata>();




    public DigitalBadge(File badge, List<DigitalBadgeMetadata> metadata) {

        this.badge = badge;
        this.metadata=metadata;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadge)) return false;
        DigitalBadge that = (DigitalBadge) o;
        return Objects.equals(getBadge(), that.getBadge()) && Objects.equals(metadata, that.metadata);
    }

    public File getBadge() {
        return badge;
    }

    public List<DigitalBadgeMetadata> getMetadata() {
        return metadata;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBadge(), metadata);
    }


    public void setBadge(File badge) {
        this.badge = badge;
    }

    public void setMetadata(List<DigitalBadgeMetadata> metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "DigitalBadge{" +
                "badge=" + badge +
                ", metadata=" + metadata +
                '}';
    }



}
