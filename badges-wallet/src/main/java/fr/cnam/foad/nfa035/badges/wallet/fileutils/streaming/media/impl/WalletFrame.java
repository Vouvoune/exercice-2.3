package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

public class WalletFrame extends AbstractImageFrameMedia<RandomAccessFile> implements WalletFrameMedia, AutoCloseable {


    private BufferedReader encodedImageReader;
    private FileOutputStream encodedImageOutput = null;
    long numberOfLines;

    private static final Logger LOG = LogManager.getLogger(AutoCloseable.class);




    public WalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);

    }

    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        if (encodedImageOutput == null){
            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            if(fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                writer.write(MessageFormat.format("{0,number,#};{1,number,#};", 1, file.getFilePointer()));
                writer.flush();
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                String[]data = lastLine.split(";");
                this.numberOfLines = Long.parseLong(data[0]) - 1;
                file.seek(fileLength);
                br.close();
            }
        }
        return encodedImageOutput;
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new BufferedInputStream(new FileInputStream(getChannel().getFD()));
    }

    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (encodedImageReader == null || !resume){
            this.encodedImageReader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel().getFD()), StandardCharsets.UTF_8));
        }
        return this.encodedImageReader;
    }

    @Override
    public long getNumberOfLines() {
        return numberOfLines;
    }

    @Override
    public void incrementLines() {
        this.numberOfLines++;
    }


    @Override
    public void close() throws Exception {
    }
}
