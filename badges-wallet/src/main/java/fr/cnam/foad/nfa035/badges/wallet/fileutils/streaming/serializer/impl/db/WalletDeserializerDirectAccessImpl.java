package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

    private final Logger LOG = LogManager.getLogger();
    private OutputStream sourceOutputStream;
    List<DigitalBadgeMetadata> metas;

    public WalletDeserializerDirectAccessImpl(OutputStream sourceOutputStream,List<DigitalBadgeMetadata> metas) {
        this.sourceOutputStream = sourceOutputStream;
        this.metas = metas;
    }


    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return (K) metas;
    }

    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException {
        long pos = metas.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }

    }

    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        this.metas= (List<DigitalBadgeMetadata>) media;
    }

    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return (T) sourceOutputStream;
    }



    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
        this.sourceOutputStream = sourceOutputStream;

    }
}
