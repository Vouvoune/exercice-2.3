package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.DatabaseMetaData;

public class WalletSerializerDirectAccessImpl extends AbstractStreamingImageSerializer {
    @Override
    public InputStream getSourceInputStream(Object source) throws IOException {
        return getSourceInputStream(source);
    }

    @Override
    public OutputStream getSerializingStream(Object media) throws IOException {
        return getSerializingStream(media) ;
    }

    @Override
    public void serialize(Object source, Object media) throws IOException {
        super.serialize(source, media);
    }
}
