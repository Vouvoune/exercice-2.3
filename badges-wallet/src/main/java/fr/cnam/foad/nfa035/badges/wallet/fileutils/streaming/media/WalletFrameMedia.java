package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.RandomAccessFile;


/**
 * Mémorisation du nombre de lignes et incrémentation des lignes
 */

public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {
   // mémoriser le nombre de lignes
   long getNumberOfLines();

   //incrémente les lignes
   void incrementLines();
}
